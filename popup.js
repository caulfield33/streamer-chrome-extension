const recording = document.getElementById('recording');
const stop = document.getElementById('stop');
const state = document.getElementById('state');

let isStreamerTsClientPresent = false;

chrome.tabs.executeScript({
    code: `
        (function() {
            return window['streamerTsClient'] !== undefined;
        })();
        `
}, (results) => {
    isStreamerTsClientPresent = results[0];
});

chrome.tabs.executeScript({
    code: `
        (function() {
        if (window['streamerTsClient'] !== undefined) {
            return window['streamerTsClient'].isRecording;
        } else {
            return false;
        }
        })();
        `
}, (results) => {
    if (results[0]) {
        state.style.background = 'red';
        recording.disabled = true;
        stop.disabled = false;
    } else {
        state.style.background = 'grey';
        recording.disabled = false;
        stop.disabled = true;
    }
});


recording.addEventListener('click', () => {

    if (!isStreamerTsClientPresent) {
        chrome.runtime.sendMessage({command: 'connect'});
    }

    setTimeout(() => {
        chrome.runtime.sendMessage({command: 'record'});
        state.style.background = 'red';
        recording.disabled = true;
        stop.disabled = false;
    }, 100)
})

stop.addEventListener('click', () => {
    chrome.runtime.sendMessage({command: 'stop'});
    state.style.background = 'grey';
    recording.disabled = false;
    stop.disabled = true;
})
