/* global chrome */

chrome.runtime.onMessage.addListener((message, sender) => {

    if (message.command === 'connect') {
        chrome.tabs.executeScript( { file: 'tracker.js' } );
    }

    if (message.command === 'record') {
        chrome.tabs.executeScript({code: "window['streamerTsClient'].startRecording()"});
    }

    if (message.command === 'stop') {
        chrome.tabs.executeScript({code: "window['streamerTsClient'].stopRecording()"});
    }
});
