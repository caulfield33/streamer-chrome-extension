class StreamerTsClient {
    constructor(key) {
        this.key = key;

        this.start = Date.now();
        this.isStream = sessionStorage.getItem('streamerTsClient');
        this._cookies = this.getCookies();

        this.socket = null;
        this.wssUrl = "localhost:5000";

        this.queue = [];

        this.streamId = this.uuid();

        this.eventListeners = [];

        this.isRecording = false;

        if (!this.isStream) {
            sessionStorage.setItem('streamerTsClient', this.streamId);
        }

        this.isStreamReady = new Promise((resolve) => this.isStreamReadyCallback = resolve);
        this.isStreamReadyCallback = null;

        this.keysToIgnore = [93];
    }

    startRecording = () => {
        this.initSocket().then(() => {
            this.isRecording = true;
            console.log('star recording')
            this.sendInit();
        }).catch((e) => {
            if (e.message === 'ChromeStreamer') {
                console.log('is stream');
            }
        });
    }

    stopRecording = () => {
        this.socket.close();
        console.log('end recording')
        this.isRecording = false;
    }

    uuid = () => {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
            let r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    };

    getCookies = () => {
        let split = document.cookie.split(';');
        let cookie = {};
        split.forEach((string) => {
            let [key, value] = string.split('=');
            cookie[key.replace(/^\s+/, '')] = value;
        });

        return cookie;
    };

    initSocket = () => {
        if (this._cookies['ChromeStreamer']) {
            return Promise.reject(new Error('ChromeStreamer'));
        }

        this.socket = new WebSocket(`wss://${this.wssUrl}`);

        this.isStreamReady = new Promise((resolve) => this.isStreamReadyCallback = resolve);
        this.socket.addEventListener('open', () => this.isStreamReadyCallback());
        this.socket.addEventListener('error', () => initSocket());

        this.socket.addEventListener('message', (e) => {
            let message = JSON.parse(e.data);
            if (message.error) {
                console.error(`Streamer Error: ${message.error}`);
                this.socket.close();
                this.socket = null;
            }
        });
        return this.isStreamReady;
    };

    sendInit = () => {
        let data = {
            event: 'init',
            width: window.innerWidth,
            height: window.innerHeight,
            start: new Date().toISOString(),
            cookies: this.getCookies(),
            localStorage: this.localResources('localStorage'),
            sessionStorage: this.localResources('sessionStorage'),
            userAgent: navigator.userAgent,
            url: this.removeUrl(window.location.href),
            id: this.streamId,
            key: this.key
        };

        this.sendAction(data);
    };

    localResources = (storage) => {
        let data = {};

        Object.keys(window[storage]).forEach((key) => {
            data[key] = window[storage].getItem(key)
        });

        return data
    };

    removeUrl = (url) => {
        let query = url.split('?')[1];

        if (query) {
            let urlParts = query.split('&');
            let parametetrs = {};
            urlParts.forEach((part) => {
                let [key, value] = part.split('=');
                parametetrs[key] = value;
            });

            delete parametetrs['gclid'];

            urlParts = [];
            for (let key in parametetrs) {
                urlParts.push(`${key}=${parametetrs[key]}`);
            }

            query = urlParts.join('&');

            return `${url.split('?')[0]}?${query}`;
        }

        return url;
    };

    sendAction = (data) => {
        if (this.isRecording) {
            if (this.socket && this.socket.readyState) {
                while (this.queue.length) {
                    socket.send(this.queue.shift());
                }
                this.socket.send(JSON.stringify(Object.assign(data, {
                    time: this.actionTime()
                })));
            } else {
                this.queue.push(JSON.stringify(Object.assign(data, {
                    time: this.actionTime()
                })));
            }
        }
    };

    actionTime = () => Date.now() - this.start;

    mouseButton = (event) => {
        let buttonMap = {
            null: 0, // nothing is pressed
            0: 1, // left mouse button
            1: 4, // middle mouse button
            2: 2, // right mouse button
            3: 8,
            4: 16
        };

        return buttonMap[event.button];
    };

    elementPosition = (el) => {
        let _x = 0;
        let _y = 0;
        while (el && !isNaN(el.offsetLeft) && !isNaN(el.offsetTop)) {
            _x += el.offsetLeft - el.scrollLeft;
            _y += el.offsetTop - el.scrollTop;
            el = el.offsetParent;
        }
        return {y: _y, x: _x};
    };

    // Actions track

    dblclick = (event) => {
        this.isStreamReady.then(() => {
            let data = {
                event: 'dblclick',
                x: event.clientX,
                y: event.clientY
            };
            this.sendAction(data);
        });
    };

    keydown = (event) => {
        this.isStreamReady.then(() => {
            let key = event.keyCode;
            if (!this.keysToIgnore.includes(key)) {
                let data = {
                    event: 'keydown',
                    key,
                };
                this.sendAction(data);
            }
        });
    };

    keyup = (event) => {
        this.isStreamReady.then(() => {
            let key = event.keyCode;
            if (!this.keysToIgnore.includes(key)) {
                let data = {
                    event: 'keyup',
                    key,
                };
                this.sendAction(data);
            }
        });
    };

    mousedown = (event) => {
        this.isStreamReady.then(() => {
            let data = {
                event: 'mousedown',
                x: event.clientX,
                y: event.clientY,
                button: this.mouseButton(event)
            };
            this.sendAction(data);
        });
    };

    mouseup = (event) => {
        this.isStreamReady.then(() => {
            let data = {
                event: 'mouseup',
                x: event.clientX,
                y: event.clientY,
                button: this.mouseButton(event)
            };
            this.sendAction(data);
        });
    };

    mousemove = (event) => {
        this.isStreamReady.then(() => {
            let data = {
                event: 'mousemove',
                x: event.clientX,
                y: event.clientY,
                b: event.buttons
            };
            this.sendAction(data);
        });
    };

    touchcancel = (event) => {
        this.isStreamReady.then(() => {
            let data = {
                event: 'touchcancel',
                x: event.clientX,
                y: event.clientY
            };
            this.sendAction(data);
        });
    };

    touchend = (event) => {
        this.isStreamReady.then(() => {
            let data = {
                event: 'touchend',
                x: event.clientX,
                y: event.clientY
            };
            this.sendAction(data);
        });
    };

    touchmove = (event) => {
        this.isStreamReady.then(() => {
            let data = {
                event: 'touchmove',
                x: event.clientX,
                y: event.clientY
            };
            this.sendAction(data);
        });
    };

    touchstart = (event) => {
        this.isStreamReady.then(() => {
            let data = {
                event: 'touchstart',
                x: event.clientX,
                y: event.clientY
            };
            this.sendAction(data);
        });
    };

    wheel = (event) => {
        this.isStreamReady.then(() => {
            let data = {
                event: 'wheel',
                x: event.deltaX,
                y: event.deltaY,
                z: event.deltaZ
            };
            this.sendAction(data);
        });
    };

    resize = () => {
        this.isStreamReady.then(() => {
            let data = {
                event: 'resize',
                width: window.innerWidth,
                height: window.innerHeight,
            };
            this.sendAction(data);
        });
    };

    scroll = () => {
        this.isStreamReady.then(() => {
            let data = {
                event: 'scroll',
                x: window.scrollX,
                y: window.scrollY,
            };
            this.sendAction(data);
        });
    }
}

function createSteamer(apiKey) {
    let streamerTsClient = new StreamerTsClient(apiKey);

    ['dblclick', 'keydown', 'keyup', 'mousedown', 'mousemove', 'mouseup', 'touchcancel', 'touchend', 'touchmove',
        'touchstart', 'wheel'].forEach((event) => {
        document.addEventListener(event, (e) => {
            streamerTsClient[event](e);
        });
    });


    let throttle = false;
    document.addEventListener('click', (event) => {
        if (!throttle && event.detail === 3) {
            let data = {
                event: 'tplclick',
                x: event.clientX,
                y: event.clientY
            };
            streamerTsClient.sendAction(data);
            throttle = true;
            setTimeout(() => (throttle = false), 300);
        }
    });


    document.addEventListener("paste", (e) => {
        let paste = (e.clipboardData || document.clipboardData).getData('text');
        streamerTsClient.sendAction({
            event: e.type,
            text: e.type === 'copy' ? document.getSelection().toString() : paste
        });
    });


    document.addEventListener('change', (e) => {
        setTimeout(() => {
            if (e.target.matches(':-webkit-autofill') && e.target.tagName === 'INPUT') {
                const coordinates = streamerTsClient.elementPosition(e.target);
                let data = {
                    event: 'autofill',
                    x: coordinates.x,
                    y: coordinates.y,
                    text: e.target.value
                };
                streamerTsClient.sendAction(data);
            }
        })
    });


    let allElements;
    const elementEvents = [];
    const elementEventsHandlers = elementEvents.map((event) => {
        return (e) => streamerTsClient[event](e);
    });

    function addEventListeners() {
        if (allElements) {
            allElements.forEach((element) => {
                elementEvents.forEach((event) => {
                    element.removeEventListener(event, elementEventsHandlers[event]);
                });
            })
        }

        allElements = [...document.getElementsByTagName('*')];
        allElements.forEach((element) => {
            elementEvents.forEach((event, i) => {
                element.addEventListener(event, elementEventsHandlers[i]);
            });
        })
    }

    // document.addEventListener('DOMContentLoaded', addEventListeners);
    // document.addEventListener('DOMSubtreeModified', addEventListeners);

    window.addEventListener('resize', streamerTsClient.resize);
    window.addEventListener('scroll', streamerTsClient.scroll);

    console.log('streamer created')

    return streamerTsClient
}

window['streamerTsClient'] = createSteamer('test')
