/* global chrome */
window.onload = () => {
    window.addEventListener('message', (event) => chrome.runtime.sendMessage(event.data));
};
